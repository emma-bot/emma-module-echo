from emma.events import Event, subscribe, trigger
from emma.module import Module
from emma.interface.message import Message


class echo(Module):
    def run(self):
        help_event = Event(event="help", identifier=self.conf['id'])
        subscribe(help_event, self.help_handler)
        cmd_event = Event(event="command", identifier=self.conf['id'])
        subscribe(cmd_event, self.cmd_handler)

    def help_handler(self, event, data):
        if not data:
            return " Echo something back.\n" \
                   " * echo message\n" \
                   "   says message."
        elif data[0] in ('echo', _('echo')):
            return "Repeat the given message"

    def cmd_handler(self, event, data):
        cmd, args = data[0]
        if cmd in ('echo', _('echo')):
            to = data[1]['From']
            if event.interface == 'irc' and data[1]['To'][0] == '#':
                to = data[1]['To']

            msg = Message(args, to)
            event = Event(event="send", interface=event.interface,
                          identifier=event.identifier)
            trigger(event, msg)
