.. contents::

Introduction
============

This emma module echoes the given information to the sender.

Not very useful, but a simple example to emma module development.

If you are looking at this package to learn emma module development, please
look at emma docs:
https://gitorious.org/emma/emma/blobs/master/docs/MODULE_TUTORIAL.rst

Installation
============

First install as always::
    $ python setup.py install

Next enable the echo module in emma config. Add those lines at the bottom
of the file::
    [M echo bar]
    id = foo

